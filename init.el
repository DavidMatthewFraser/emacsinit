;; Melpa packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
;; use-package package manager
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
;;This sets $MANPATH, $PATH and exec-path from shell
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

(org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org"))

